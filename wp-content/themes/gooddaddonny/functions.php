<?php

// loading css stylesheets
function load_stylesheets()
{

  wp_register_style('bootstrap', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css', array(), false, 'all');
  wp_enqueue_style('bootstrap');

  wp_register_style('fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css', array(), false, 'all');
  wp_enqueue_style('fontawesome');

  wp_register_style('aos', 'https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css', array(), false, 'all');
  wp_enqueue_style('aos');

  wp_register_style('stylesheet', get_template_directory_uri() . '/style.css', array(), false, 'all');
  wp_enqueue_style('stylesheet');

}
add_action('wp_enqueue_scripts', 'load_stylesheets');

//loading JS scripts
function load_js()
{

  wp_deregister_script('jquery');

  wp_register_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js', '', 1, true);
  wp_enqueue_script('jquery');

  wp_register_script('bootstrapjs', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js', '', 1, true);
  wp_enqueue_script('bootstrapjs');

  wp_register_script('aosjs', 'https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js', '', 1, true);
  wp_enqueue_script('aosjs');
  
}
add_action('wp_enqueue_scripts', 'load_js');

// for meta tag for viewport
function add_viewport_meta_tag()
{
  echo '<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">';
}
add_action('wp_head', 'add_viewport_meta_tag', '1');


//Use Old Wordpress Editor
add_filter('use_block_editor_for_post', '__return_false', 10);

function custom_post_type()
{

  // testimonials
    $args = array(

        'labels' => array(
            'name' => 'Testimonials',
            'singular' => 'testimonial',
        ),
        'hierarchical' => 'true',
        'public' => true,
        'has_archive' => true,
        'menu_icon' => 'dashicons-testimonial',
        'supports' => array('title', 'thumbnail'),

  );
  register_post_type('testimonial', $args);


  // faqs
  $args = array(

    'labels' => array(
        'name' => 'Faq',
        'singular' => 'faq',
    ),
    'hierarchical' => 'true',
    'public' => true,
    'has_archive' => true,
    'menu_icon' => 'dashicons-editor-spellcheck',
    'supports' => array('title', 'thumbnail'),

  );
  register_post_type('faq', $args);

}
add_action('init', 'custom_post_type');

?>