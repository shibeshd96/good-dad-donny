<?php /* Template Name: About Page */ ?>

<?php get_header();?>

    <section id="about-donny">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <?php the_content();?>
                </div>
            </div>
        </div>
    </section>

<?php get_footer();?>