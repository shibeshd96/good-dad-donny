<?php wp_footer(); ?>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 col-md-12">
                        <div class="info">
                            <h3><strong>GET IN TOUCH</strong></h3>
                            <a href="mailto:4tuples@gmail.com">example@email.com</a>
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-12">
                        <h3 class="text-right">Want to know who the real Donny is?</h3>
                        <a href="<?php the_permalink(176);?>" class="d-block text-right">Read His Story!</a>
                        <!-- <div class="contact-form">
                            <?php //echo do_shortcode('[contact-form-7 id="8" title="Contact form 1"]');?>
                        </div> -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <span class="footer-logo text-center">Good Dad Donny</span>
                        <span class="copyright text-center">@ 2020 Good Dad Donny - All Rights Reserved</span>
                    </div>
                </div>
            </div>
            <button id="backtotop" title="Go to top"><i class="fas fa-long-arrow-alt-up"></i></button>
        </footer>
        <script>
            $(window).on('load', function() {
                $('.btn-link').on('click', function () {
                    var all = $('.fa-plus').filter('.active');

                    if ($(this).attr('aria-expanded') != "true") {
                        $.each(all, function (index, value) {
                            $(value).toggleClass('active');
                            $(value).toggleClass('notactive');
                        });
                    }

                    if ($(this).attr('aria-expanded') === "true") {
                        $(this).children('.fa-plus').toggleClass('active');
                        $(this).children('.fa-plus').toggleClass('notactive');
                    } else {
                        $(this).children('.fa-plus').toggleClass('active');
                        $(this).children('.fa-plus').toggleClass('notactive');
                    }
                });

                $("#backtotop").on('click', function() {
                    document.body.scrollTop = 0;
                    document.documentElement.scrollTop = 0;
                })
                
                AOS.init();
            });
        </script>
    </body>
</html>
