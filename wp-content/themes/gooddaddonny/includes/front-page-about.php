<?php
    if( have_rows('about_section') ):
    while( have_rows('about_section') ): the_row();

    $obj = get_field_object('about_section'); 
    $title = get_sub_field('title');
    $description = get_sub_field('description');
    $image = get_sub_field('image');
?>

<section id="about">
    <div class="container position-relative">
        <h2 class="text-center" data-aos="fade-up" data-aos-duration="1000"><?php echo $title;?></h2>
        <div class="row">
            <div class="col-12">
                <img src="<?php echo $image;?>" class="d-block m-auto" />
                <span data-aos="zoom-in" data-aos-duration="1000"><?php echo $description;?></span>
            </div>
            <!-- <div class="col-lg-7 col-md-12 ">
                <img src="<?php echo $image;?>" />
            </div>
            <div class="col-lg-5 col-md-12">
                <span data-aos="zoom-in" data-aos-duration="1000"><?php echo $description;?></span>
            </div> -->
        </div>
        <div class="background"></div>
    </div>
</section>

<?php endwhile; endif; wp_reset_query();?>