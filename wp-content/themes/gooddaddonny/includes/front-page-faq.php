<section id="faq">
    <div class="container">
        <h3 class="pb-4">Questions? Here are the answers:</h3>
        <?php
            $faqs = new WP_Query(
                array(
                    'post_type' => 'faq',
                    'posts_per_page' => -1,
                    'order_by' => 'date',
                    'order' => 'ASC',
                )
            );

        if ($faqs->have_posts()) : while ($faqs->have_posts()) : $faqs->the_post();
        ?>

        <div class="card">
            <div class="card-header" id="faq-<?php echo get_the_ID();?>">
                <h5 class="mb-0">
                    <button class="btn btn-link w-100 text-left collapsed" data-toggle="collapse" data-target="#collapse-<?php echo get_the_ID();?>" aria-expanded="false" aria-controls="collapseOne">
                        <span><?php echo the_title();?></span>
                        <i class="fas fa-plus notactive"></i>
                    </button>
                </h5>
            </div>

            <div id="collapse-<?php echo get_the_ID();?>" class="collapse" aria-labelledby="faq-<?php echo get_the_ID();?>" data-parent="#faq">
                <div class="card-body">
                    <?php the_field('answer');?>
                </div>
            </div>
        </div>

        <?php endwhile; endif; wp_reset_query();?>
    </div>
</section>