<?php
    if( have_rows('steps_section') ):
    while( have_rows('steps_section') ): the_row();

    $obj = get_field_object('steps_section'); 
    $title = get_sub_field('title');
    $step1 = get_sub_field('step_1');
    $step1image = get_sub_field('step_1_image');
    $step2 = get_sub_field('step_2');
    $step2image = get_sub_field('step_2_image');
    $step3 = get_sub_field('step_3');
    $step3image = get_sub_field('step_3_image');
?>

    <section id="steps">
        <div class="container">
            <h2 class="text-center" data-aos="fade-up" data-aos-duration="1000"><?php echo $title;?></h2>
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-6 col-sm-12 position-relative" data-aos="fade-down-right" data-aos-duration="1000">
                    <img src="<?php echo $step1image;?>" class="steps-img"/>
                    <span><?php echo $step1;?></span>
                    <span class="number">1</span>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 position-relative" data-aos="fade-up" data-aos-duration="1000">
                    <img src="<?php echo $step2image;?>" class="steps-img"/>
                    <span><?php echo $step2;?></span>
                    <span class="number">2</span>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12 position-relative" data-aos="fade-down-left" data-aos-duration="1000">
                    <img src="<?php echo $step3image;?>" class="steps-img"/>
                    <span><?php echo $step3;?></span>
                    <span class="number">3</span>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; endif; wp_reset_query();?>