<?php
    if( have_rows('why_quiz') ):
    while( have_rows('why_quiz') ): the_row();

    $obj = get_field_object('why_quiz'); 
    $title = get_sub_field('title');
    $image = get_sub_field('image');
?>

    <section id="whyquiz">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <img src="<?php echo $image;?>"/>
                    <div class="content">
                        <?php echo $title;?>
                        <a href="<?php the_permalink(172);?>"><button class="cta">Get Started</button></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; endif; wp_reset_query();?>