<?php
    if( have_rows('testimonial_section') ):
    while( have_rows('testimonial_section') ): the_row();

    $obj = get_field_object('testimonial_section'); 
    $title = get_sub_field('title');
?>

<section id="testimonial">
    <div class="container">
        <h2 class="text-center"><?php echo $title;?></h2>
        <span class="divider"></span>
        <div class="row justify-content-center">
            <?php
                $testimonial = new WP_Query(
                    array(
                        'post_type' => 'testimonial',
                        'posts_per_page' => 3,
                        'order_by' => 'date',
                        'order' => 'ASC'
                    )
                );

            if ($testimonial->have_posts()) : while ($testimonial->have_posts()) : $testimonial->the_post();
            $rating = get_field_object('rating');
            $num = (int) $rating['value'];
            ?>

                    <div class="col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="custom-cards">
                            <div class="description"><?php the_field('testimonial');?></div>
                            <div class="row pt-4">
                                <div class="author col-6">— <?php the_title();?></div>
                                <div class="rating col-6 text-right">
                                    <?php for ($i = 0; $i < $num; $i++) { ?>
                                        <span data-aos="fade-right"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/star.png"/></span>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </div>

            <?php endwhile; endif; wp_reset_query();?>
        </div>
    </div>

</section>

<?php endwhile; endif; wp_reset_query();?>