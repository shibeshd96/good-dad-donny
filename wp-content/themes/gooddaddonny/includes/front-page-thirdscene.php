<?php
    if( have_rows('third_scene') ):
    while( have_rows('third_scene') ): the_row();

    $obj = get_field_object('third_scene'); 
    $title = get_sub_field('title');
    $image = get_sub_field('image');
?>

<section id="thirdscene">
    <div class="container">
        <h4 class="cta-text py-4"><?php echo $title;?></h4>
    </div>
    <img src="<?php echo $image;?>" data-aos="fade-right" data-aos-duration="1000"/>
</section>

<?php endwhile; endif; wp_reset_query();?>