<?php
    if( have_rows('banner_section') ):
    while( have_rows('banner_section') ): the_row();

    $obj = get_field_object('banner_section'); 
    $tagline = get_sub_field('banner_tagline');
    $description = get_sub_field('description');
    $image = get_sub_field('banner_image');
?>

        <section id="banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12" data-aos="fade-up" data-aos-duration="1000">
                        <?php echo $description;?>
                        <a href="<?php the_permalink(172);?>"><button class="cta">Get Started</button></a>
                    </div>
                    <div class="col-md-8 col-sm-12 text-center" data-aos="fade-down" data-aos-duration="1500">
                        <img src="<?php echo $image;?>" class="img-banner"/>
                    </div>
                </div>
            </div>
        </section>

<?php endwhile; endif; wp_reset_query();?>