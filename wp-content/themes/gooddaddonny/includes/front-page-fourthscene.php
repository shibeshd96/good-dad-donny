<?php
    if( have_rows('fourth_scene') ):
    while( have_rows('fourth_scene') ): the_row();

    $obj = get_field_object('fourth_scene'); 
    $title = get_sub_field('title');
    $image = get_sub_field('image');
?>

<section id="fourthscene">
    <div class="container">
        <h4 class="py-4"><?php echo $title;?></h4>
    </div>
    <img src="<?php echo $image;?>" data-aos="fade-right" data-aos-duration="1000"/>
    <div class="container">
        <a href="<?php the_permalink(172);?>"><button class="cta">Get Started</button></a>
    </div>
</section>

<?php endwhile; endif; wp_reset_query();?>