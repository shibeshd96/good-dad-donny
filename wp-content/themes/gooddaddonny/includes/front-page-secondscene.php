<?php
    if( have_rows('second_scene') ):
    while( have_rows('second_scene') ): the_row();

    $obj = get_field_object('second_scene'); 
    $title = get_sub_field('title');
    $image = get_sub_field('image');
?>

    <section id="secondscene">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-12">
                    <h4 class="pt-4" data-aos="fade-up" data-aos-duration="1000"><?php echo $title;?></h4>
                    <a href="<?php the_permalink(172);?>"><button class="cta">Get Started</button></a>
                </div>
                <div class="col-md-4 col-12">
                    <img src="<?php echo $image;?>" data-aos="fade-down" data-aos-duration="1000"/>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; endif; wp_reset_query();?>