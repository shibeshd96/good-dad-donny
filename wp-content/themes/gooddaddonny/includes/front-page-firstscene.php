<?php
    if( have_rows('first_scene') ):
    while( have_rows('first_scene') ): the_row();

    $obj = get_field_object('first_scene'); 
    $title = get_sub_field('title');
    $image = get_sub_field('image');
    $description = get_sub_field('description');
?>

    <section id="firstscene">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-12">
                    <img src="<?php echo $image;?>" data-aos="fade-down" data-aos-duration="1000"/>
                </div>
                <div class="col-md-5 col-12 d-flex align-items-center">
                    <h4 class="py-4" data-aos="fade-up" data-aos-duration="1000"><?php echo $title;?></h4>
                </div>
            </div>
        </div>
        <div class="background" style="background-image: url('<?php echo get_stylesheet_directory_uri();?>/assets/images/firstscenebackground.png')">
            <div class="container">
                <div class="row py-4">
                    <div class="col-12">
                        <h5 class="description py-4"><?php echo $description;?></h5>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; endif; wp_reset_query();?>