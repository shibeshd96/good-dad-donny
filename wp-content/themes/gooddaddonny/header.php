<?php wp_head(); ?>
<html>

<head>
    <title>Good Dad Donny</title>
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/favicon.ico?v=2" type="image/x-icon">
    <link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/favicon.ico?v=2" type="image/x-icon">

    <!-- Fonts -->
    <!-- <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Allerta+Stencil&display=swap" rel="stylesheet">
</head>
<body>
    <header>
        <div class="container">
            <div class="row">
                <div class="header-logo col-md-9 col-12">
                    <a href="<?php echo(site_url('/')); ?>" class="d-block">
                        <!-- <img src="<?php //echo get_stylesheet_directory_uri();?>/assets/logo.png" width="auto" height="auto" class="desktop-logo"> -->
                        <div class="logo-text">Good Dad Donny</div>
                    </a>
                </div>
				<div class="col-md-3 col-12">
					<a href="<?php the_permalink(225);?>" class="cta" style="display: block; margin-top: 28px;">Get Your Results!</a>
				</div>
            </div>
        </div>
    </header>