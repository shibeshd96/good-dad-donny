<?php get_header();?>

    <?php get_template_part('includes/front-page', 'banner'); ?>
    <?php get_template_part('includes/front-page', 'testimonial'); ?>
    <?php get_template_part('includes/front-page', 'firstscene'); ?>
    <?php get_template_part('includes/front-page', 'secondscene'); ?>
    <?php get_template_part('includes/front-page', 'about'); ?>
    <?php get_template_part('includes/front-page', 'whyquiz'); ?>
    <?php get_template_part('includes/front-page', 'steps'); ?>
    <?php get_template_part('includes/front-page', 'thirdscene'); ?>
    <?php get_template_part('includes/front-page', 'faq'); ?>
    <?php get_template_part('includes/front-page', 'fourthscene'); ?>

<?php get_footer();?>